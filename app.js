var maexClient = require('./client/client.js');
var protocolHandler = require('./protocol/protocolHandler.js');
var gamestate = require('./model/gamestate.js');
var dieTables = require('./data/dieTables.js');


const MIN_TRUTH_PROBABILITY_TO_ACCEPT = 0.25;
const TRUTH_PROBABILITY_OFFSET = 0.0; //Used to change behavior of lie detection.
const TRUTH_PROBABILITY_OFFSET_IF_DIRECT_NEIGHBOURGH = -0.15;
const TRUTH_PROBABILITY_OFFSET_IF_NEIGHBOURGH_TWO_REMOVED = -0.05;
const TRUTH_PROBABILITY_OFFSET_IF_NEIGHBOURGH_THREE_REMOVED = -0.025;
const TRUTH_PROBABILITY_OFFSET_FIRST_PLAYER = 0.35;



var splitMessage = function(message){
    return message.split(';');
};

var turnDieToInt = function (die){
    var i =   parseInt(die.split(",").join(''));
    return i;
};

var turnIntToDie = function(int){
     var tempDie =  int.toString();
     return tempDie[0]+','+tempDie[1];
};

var dieIsHigherOrEqual = function (die1, die2){
   return dieTables.higherOrEqual[die1][die2];
};

var shouldRoll = function(){

    var lastDie = gamestate.diceRolls[gamestate.diceRolls.length-1];
    var dieBeforeLast = gamestate.diceRolls[gamestate.diceRolls.length-2];


    if(lastDie == 66 || lastDie == 55){
        return false;
    }


    function announcedDieToPreviousDieProximityProbabilityChange(){
        if(dieTables.higherDie[dieBeforeLast][0] == lastDie )
            return TRUTH_PROBABILITY_OFFSET_IF_DIRECT_NEIGHBOURGH;
        if(dieTables.higherDie[dieBeforeLast][1] == lastDie )
            return TRUTH_PROBABILITY_OFFSET_IF_NEIGHBOURGH_TWO_REMOVED;
        if(dieTables.higherDie[dieBeforeLast][2] == lastDie){
            return TRUTH_PROBABILITY_OFFSET_IF_NEIGHBOURGH_THREE_REMOVED;
        }
        return 0.0;
    }

    function firstPlayerProbabilityChange(){
        if(gamestate.diceRolls.length == 2)
            return TRUTH_PROBABILITY_OFFSET_FIRST_PLAYER;
        return 0.0;
    }

    function hasLikelyLied(){
        var truthProb = dieTables.probabilityToGetOver[dieBeforeLast];

        truthProb += announcedDieToPreviousDieProximityProbabilityChange();
        truthProb += firstPlayerProbabilityChange();
        truthProb += TRUTH_PROBABILITY_OFFSET;

        console.log("Truth prob: " + truthProb);
        return truthProb < MIN_TRUTH_PROBABILITY_TO_ACCEPT;
    }

    return !hasLikelyLied();

};

var shouldBluff = function(currentDie){
    var lastDie = gamestate.diceRolls[gamestate.diceRolls.length-1];
    return dieIsHigherOrEqual(currentDie, lastDie);
};

var getBluff = function(){
    var lastDie = gamestate.diceRolls[gamestate.diceRolls.length-1];
    var dieIndexToUse = 0;
    if(dieTables.higherDie[lastDie].length > 5){
        var ran = Math.random();
        if (ran < 0.5){
            dieIndexToUse = 1;
        }
        else if(ran < 0.75){
            dieIndexToUse = 2;
        }
        else if(ran < 0.9){
            dieIndexToUse = 3;
        }
    }

    return dieTables.higherDie[lastDie][dieIndexToUse];
};

protocolHandler.on(protocolHandler.events.REGISTERED, function(){
    //console.log("Am registered!");
});
protocolHandler.on(protocolHandler.events.ROUND_CANCELED, function(message){
   // console.log("Round was Canceled: " + message);
});
protocolHandler.on(protocolHandler.events.ROUND_STARTING, function(message){
   // console.log("Round Started, going to Join: " + message);
    gamestate.diceRolls = [32];
    var parts = splitMessage(message);
    maexClient.join(parts[1]);
});
protocolHandler.on(protocolHandler.events.YOUR_TURN, function(message){
   //console.log("Taking Turn" + message);
   var parts = splitMessage(message);

   if(gamestate.diceRolls.length == 1){
       maexClient.roll(parts[1]);
   }else{
       if(shouldRoll()){
           maexClient.roll(parts[1]);
       } else {
           maexClient.see(parts[1]);
       }
   }
});

protocolHandler.on(protocolHandler.events.ANNOUNCED, function(message){
    var parts = splitMessage(message);
    gamestate.diceRolls.push(turnDieToInt(parts[2]));
});

protocolHandler.on(protocolHandler.events.ROLLED, function(message){
    var parts = splitMessage(message);
    var currentDie = turnDieToInt(parts[1]);
    if(shouldBluff(currentDie)){
        currentDie = getBluff();
    }
    maexClient.announce(turnIntToDie(currentDie), parts[2]);
});

maexClient.onMessage(function(message){
   console.log(''+message);
   protocolHandler.handleMessages(message);
});

maexClient.register();

