var config = require('../config.js');
var registerMessages = require('../protocol/registerMessages.js');
var clientActionMessages = require('../protocol/clientActionMessages.js');
var dgram = require('dgram');


var lowLevelclient = dgram.createSocket('udp4');
lowLevelclient.bind(config.CLIENTPORT);
var client = {};

client.sendMessage = function(message, callback){
    lowLevelclient.send(message,0, message.length, config.SERVERPORT, config.SERVERHOST, callback);
};
client.close = lowLevelclient.close;

client.register = function(){
    this.sendMessage(new Buffer(registerMessages.registerAsPlayer));
};

client.join = function(token){
    this.sendMessage(new Buffer(clientActionMessages.join+token));
};

client.see = function(token){
    this.sendMessage(new Buffer(clientActionMessages.see+token));
};

client.announce = function(dice,token){
    this.sendMessage(new Buffer(clientActionMessages.announce+dice+";"+token));
};

client.roll = function(token){
    this.sendMessage(new Buffer(clientActionMessages.roll+token));
};

client.onMessage = function(messageCallback){
    lowLevelclient.on('message', messageCallback);
};

module.exports = client;