var events = require('events');

var registerMessages = require('./registerMessages.js');
var roundOrderMessages = require('./roundOrderMessages.js');
var gameProcessMessages = require('./gameProcessMessages.js');

const eventNames = {
    REGISTERED: "registered",
    REJECTED: "rejected",
    ROUND_STARTING: "roundStarting",
    ROUND_CANCELED: "roundCanceled",
    ROLLED: "rolled",
    ANNOUNCED: "announced",
    YOUR_TURN: "yourturn"

};

function startsWith(stringToCheck, stringToCompare){
    return stringToCheck.lastIndexOf(stringToCompare,0) === 0;
}

var protocolEmitter = new events.EventEmitter();
protocolEmitter.emitRegistered = function(message){
    this.emit(eventNames.REGISTERED, message);
};
protocolEmitter.emitRejected = function(message){
    this.emit(eventNames.REJECTED, message);
};
protocolEmitter.emitRoundStarting = function(message){
    this.emit(eventNames.ROUND_STARTING, message);
};
protocolEmitter.emitRoundCanceled = function(message){
    this.emit(eventNames.ROUND_CANCELED, message);
};
protocolEmitter.emitAnnounced = function(message){
    this.emit(eventNames.ANNOUNCED, message);
};
protocolEmitter.emitRolled = function(message){
    this.emit(eventNames.ROLLED, message);
};

protocolEmitter.emitYourTurn = function(message){
    this.emit(eventNames.YOUR_TURN, message);
};



var protocolHandler = {
    events: eventNames,
    on: protocolEmitter.on.bind(protocolEmitter),
    handleMessages: function(buffer) {
        var message = buffer.toString('ascii');

        var messageStartsWith = startsWith.bind(this, message);

        if(messageStartsWith(gameProcessMessages.answerYourTurn)) {
            console.log("In If");
            protocolEmitter.emitYourTurn(message);
        } else if (messageStartsWith(gameProcessMessages.answerRolled)) {
            protocolEmitter.emitRolled(message);
        } else if (messageStartsWith(gameProcessMessages.answerAnnounced)) {
            protocolEmitter.emitAnnounced(message);
        } else if(messageStartsWith(registerMessages.answerRegistered)){
            protocolEmitter.emitRegistered(message);
        } else if (messageStartsWith( registerMessages.answerRejected)){
            protocolEmitter.emitRejected(message);
        } else if (messageStartsWith( roundOrderMessages.answerRoundStarting)){
            protocolEmitter.emitRoundStarting(message);
        } else if (messageStartsWith (roundOrderMessages.answerRoundCanceled)){
            protocolEmitter.emitRoundCanceled(message);
        }

    }
};

module.exports = protocolHandler;
