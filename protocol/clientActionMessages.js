
var clientActionMessages = {
    roll: "ROLL;",
    see: "SEE;",
    join: "JOIN;",
    announce: "ANNOUNCE;"
};

module.exports = clientActionMessages;