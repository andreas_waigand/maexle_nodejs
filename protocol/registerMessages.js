var config = require('../config.js');

var registerMessages = {
    registerAsPlayer: "REGISTER;"+config.NAME,
    registerAsSpectator: "REGISTER_SPECTATOR;"+config.NAME,
    answerRegistered: "REGISTERED",
    answerRejected: "REJECTED"
}

module.exports = registerMessages;