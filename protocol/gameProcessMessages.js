var gameProcessMessages = {
    answerAnnounced: "ANNOUNCED;",
    answerRolled: "ROLLED;",
    answerYourTurn: "YOUR TURN;"
};

module.exports = gameProcessMessages;