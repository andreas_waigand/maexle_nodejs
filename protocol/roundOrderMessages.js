var roundOrderMessages = {
    answerRoundStarting : "ROUND STARTING;",
    answerRoundStarted: "ROUND STARTED;",
    answerRoundCanceled: "ROUND CANCELED;"
}

module.exports = roundOrderMessages;


